import { Logger, ValidationPipe, VersioningType } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import * as compression from 'compression'
import helmet from 'helmet'

// bootstrap all application
// service, module and config
async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.useGlobalPipes(new ValidationPipe())
  app.enableVersioning({ type: VersioningType.URI })
  app.use(compression())
  app.use(helmet())
  app.enableCors()

  const configService = app.get(ConfigService)
  const PORT = configService.get<number>('port')

  await app.listen(PORT, () => {
    new Logger().log(`🚀 Start the application on http://localhost:${PORT}`)
  })
}

bootstrap()
