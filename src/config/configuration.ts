export interface AppConfig {
  port: number
}

// the application type of configuration
const appConfig: () => AppConfig = () => ({
  port: parseInt(process.env.PORT, 10) || 4000,
})

export { appConfig }
